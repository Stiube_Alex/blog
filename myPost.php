<?php
session_start();
require 'script/db.php';
include 'html/header.html';
include 'tools/navbar.php';
include 'html/myPost.html';

$user_id = $_SESSION['id'];
$query = "SELECT * FROM posts JOIN articles ON articles.id = article_id WHERE user_id = '$user_id' ORDER BY created_at DESC";
$result = mysqli_query($db, $query);
if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
        $article_name = $row['title'];
        $post_title = $row['title_'];
        $post_date = $row['created_at'];
        $post_content = $row['content'];
        $post_id = $row['id_'];
?>
        <div class="content m-auto border border-dark p-2 mb-3">
            <h5 class="title"><?php echo $post_title ?></h5>
            <p class="date mb-0 mt-1"><?php echo $article_name ?></p>
            <p class="date mb-0 mt-1"><?php echo $post_date ?></p>
            <div class="border-top border-dark myHr my-1"></div>
            <p class="content my-2"><?php echo $post_content ?></p>
            <div class='deletePost'>
                <a class='postDel btn btn-sm btn-outline-dark'><i class='far fa-trash-alt'></i></a>
                <a class='btn btn-sm btn-outline-dark postDelNo'>No</a>
                <a class='btn btn-sm btn-outline-danger postDelYes' data-post_id=<?php echo $post_id; ?>>Yes</a>
            </div>
        </div>
<?php
    }
}

include 'tools/tools.html';
include 'html/footer.html';
