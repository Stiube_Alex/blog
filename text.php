<?php
session_start();
require 'script/db.php';
include 'html/header.html';
include 'tools/navbar.php';
$title = $_GET['title'];
?>
<div class="container">
    <h1 class="h1 text-center m-2"> <?php echo $title; ?> </h1>
    <?php
    if (isset($_SESSION['email'])) {
    ?>
        <p class="createTextSlide mx-auto mt-2 mb-3 border border-dark"><i class="downArr far fa-caret-square-down"></i><i class="upArr far fa-caret-square-up"></i>&nbsp; Add some text in this article!</p>
    <?php
    }
    ?>
    <div class="createPost mb-3 m-auto">
        <div class="content m-auto mt-2">
            <input type="text" placeholder="Title" class="inputTitle textForms w-100">
        </div>
        <div class="content m-auto mt-2">
            <textarea class="textareaContent textForms w-100" placeholder="Text" rows="7"></textarea>
        </div>
        <div class="content m-auto">
            <a class="addText btn btn-sm btn-outline-dark">Add</a>
            <a class="clearText btn btn-sm btn-outline-dark mx-1">Clear</a>
        </div>
    </div>
    <?php

    $query = "SELECT * FROM posts JOIN users ON user_id = users.id ORDER BY created_at DESC";
    $result = mysqli_query($db, $query);
    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $article_id = $row['article_id'];
            $page_id = $_GET['id'];
            if ($article_id == $page_id) {
                $post_title = $row['title_'];
                $post_user = $row['name'];
                $post_date = $row['created_at'];
                $post_content = $row['content'];
    ?>
                <div class="content m-auto border border-dark p-2 mb-3">
                    <h5 class="title"><?php echo $post_title ?></h5>
                    <p class="user mb-0 mt-0"><?php echo $post_user ?></p>
                    <p class="date mb-0 mt-1"><?php echo $post_date ?></p>
                    <div class="border-top border-dark myHr my-1"></div>
                    <p class="content my-2"><?php echo $post_content ?></p>
                </div>
    <?php
            }
        }
    }
    ?>

</div>
<?php
include 'html/footer.html';
include 'tools/tools.html';
