//Article Post
$('.submitForm').click(function () {
    if ($(this).prev().val() && $(this).prev().prev().prev().val()) {
        $.post('script/createArticle.php', {
            title: $(this).prev().val(),
            about: $(this).prev().prev().prev().val()
        }).done(() => {
            window.location.reload();
        })
    }
})
$('.articleDelYes').click(function () {
    $.post('script/deleteArticle.php', {
        id: $(this)[0].dataset.article_id
    }).done(function () {
        window.location.reload();
    })
})
//Text Post
$('.addText').click(function () {
    if ($('.inputTitle').val() && $('.textareaContent').val()) {
        let urlParams = new URLSearchParams(window.location.search);
        console.log(urlParams.get('id'));
        $.post('script/createText.php', {
            textTitle: $('.inputTitle').val(),
            textContent: $('.textareaContent').val(),
            article_id: urlParams.get('id')
        }).done(() => {
            window.location.reload();
        })
    }
})
//My Posts Post
$('.postDelYes').click(function () {
    $.post('script/deletePost.php', {
        id_: $(this)[0].dataset.post_id
    }).done(function () {
        window.location.reload();
    })
})