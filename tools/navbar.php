<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container-fluid">
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
			<img class="logo" src="logo.png">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				<li class="nav-item">
					<a class="nav-link" aria-current="page" href="index.php">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="article.php">Articles</a>
				</li>
				<?php
				if (isset($_SESSION['email'])) {
				?>
					<li class="nav-item">
						<a class="nav-link" href="myPost.php">My Posts</a>
					</li>
				<?php
				}
				?>
			</ul>
			<?php
			if (isset($_SESSION['email'])) {
				echo '<div class="mr-4"><p class="loginMessage text-light"> Hello, ' . $_SESSION['user'] . ' </p></div>';
				echo '
				<form class="m-0 p-0" method="POST" action="script/logout.php">
					<button type="submit" name="logout" class="btn btn-outline-light btn-sm">Logout</button>
				</form>';
			} else {
			?>
				<a href="user.php" class="btn btn-outline-light btn-sm mr-1 ml-auto">Get started</a>
			<?php }
			?>
		</div>
	</div>
</nav>
<style>
	.loginMessage {
		margin: 0;
		margin-right: 15px;
	}
</style>