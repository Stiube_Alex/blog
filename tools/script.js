// user.php
$('.signUp').click(function () {
    $('.regForm').show();
    $('.logForm').hide();
})

$('.backSignUp').click(function () {
    $('.logForm').show();
    $('.regForm').hide();
})

//article.php
let clicks = 1;
$('.showForm').click(function () {
    if (clicks % 2 != 0) {
        $('.createArticleForm').show();
        clicks++;
    } else {
        $('.createArticleForm').hide();
        clicks++;
    }
})
let clickDelArticle = 1;
$('.adminArticle').click(() => {
    if (clickDelArticle % 2 != 0) {
        $('.deleteArticle').show();
        clickDelArticle++;
    } else {
        $('.deleteArticle').hide();
        clickDelArticle++;
    }
})
$('.articleDel').click(function () {
    $(this).hide();
    $(this).next().show();
    $(this).next().next().show();
})
$('.articleDelNo').click(function () {
    $(this).hide();
    $(this).prev().show();
    $(this).next().hide();
})

//text.php
$('.clearText').click(() => {
    document.querySelector('.textareaContent').value = '';
    document.querySelector('.inputTitle').value = '';
})
let clickTextSlide = 1;
$('.createTextSlide').click(() => {
    if (clickTextSlide % 2 != 0) {
        $('.createPost').show();
        $('.upArr').show();
        $('.downArr').hide();
        clickTextSlide++;
    } else {
        $('.createPost').hide();
        $('.downArr').show();
        $('.upArr').hide();
        clickTextSlide++;
    }
})
//myPost.php
let clickPostDel = 1;
$('.myPostDelete').click(() => {
    if (clickPostDel % 2 != 0) {
        $('.deletePost').show();
        clickPostDel++;
    } else {
        $('.deletePost').hide();
        clickPostDel++;
    }
})
$('.postDel').click(function () {
    $(this).hide();
    $(this).next().show();
    $(this).next().next().show();
})
$('.postDelNo').click(function () {
    $(this).prev().show();
    $(this).hide();
    $(this).next().hide();
})
