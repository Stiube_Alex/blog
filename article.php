<?php
session_start();
require 'script/db.php';
include 'html/header.html';
include 'tools/navbar.php';
?>
<div class="container">
    <div class="jumbotron p-3 p-md-5 my-5 text-white bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">Articles</h1>
            <p class="lead my-3">Choose or create an article for your purpose. You can visit freely all articles but you have to be logged in to create one or to comment something!</p>
            <?php
            if (isset($_SESSION['email'])) {
                $isAdmin = $_SESSION['role'];
                if ($isAdmin) {
                    echo '<p class="lead mb-0 showForm"><a class="link-light">Create an article...</a></p>';
                    echo '<p class="adminArticle lead mb-0"><a class="link-light">...or delete one.</a></p>';
                }
            }
            ?>
        </div>
    </div>

    <div class="col-11 col-md-6 m-auto createArticleForm mb-4">
        <label for="exampleFormControlInput1" class="form-label my-2">Topic Name</label>
        <input name="title" type="text" class="form-control" placeholder="Cars" required autocomplete="off">
        <label for="exampleFormControlTextarea1" class="form-label my-2">What is about?</label>
        <textarea name="about" class="form-control" rows="2" autocomplete="off" required placeholder="A topic about cars and things related to cars"></textarea>
        <button class="submitForm btn btn-outline-dark mt-3"">Create</button>
        </div>

        <div class=" row justify-content-around">
            <?php
            $query = "SELECT * FROM articles";
            $result = mysqli_query($db, $query);
            if ($result) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $id = $row['id'];
                    $title = $row['title'];
                    $about = $row['about'];
            ?>
                    <div class='col-11 col-sm-12 col-md-5 col-md-offset-2 border border-dark p-4 my-3 w-20'>
                        <h3 class='h3'><a href="text.php?title=<?php echo $title; ?>&id=<?php echo $id; ?>" class='articleTitle link-dark'><?php echo $title; ?></a></h3>
                        <p><?php echo $about; ?></p>

                        <div class='deleteArticle'>
                            <a class='articleDel btn btn-sm btn-outline-dark'><i class='far fa-trash-alt'></i></a>
                            <a class='btn btn-sm btn-outline-dark articleDelNo'>No</a>
                            <a class='btn btn-sm btn-outline-danger articleDelYes' data-article_id=<?php echo $id; ?>>Yes</a>
                        </div>
                    </div>

            <?php
                }
            }
            ?>
    </div>
</div>

<?php
include 'html/footer.html';
include 'tools/tools.html';
?>